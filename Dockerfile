# Dockerfile to use with heroku
# Build using:
#  docker build --rm -t instant-salon-ui .
#
#
FROM node:8

MAINTAINER Herald Olivares<heraldmatias.oz@gmail.com>

RUN mkdir -p /opt/instantsalon_ui/
COPY . /opt/instantsalon_ui/.
WORKDIR /opt/instantsalon_ui

RUN npm install
RUN npm run build


# THIS ONLY FOR LOCAL TESTING
# EXPOSE 8080

CMD npm start
