import {HTTP} from '../common/common'

export default {
  data(){
    return {
      token: localStorage.token
    }
  },

  get_services (searchName, cb) {
    cb = arguments[arguments.length - 1]
    // verify is authenticated
    var endpoint = `services/`
    if(searchName){
      endpoint += `?name=` + searchName
    }
  console.log(endpoint)
    HTTP.get(endpoint)
      .then(response => {
        cb(null, response.data)
      })
      .catch(e => {
        cb(e, null)
      })
  },

  get_service (id, cb) {
    cb = arguments[arguments.length - 1]
    // verify is authenticated
    var endpoint = `services/` + id

    HTTP.get(endpoint)
      .then(response => {
        cb(null, response.data)
      })
      .catch(e => {
        cb(e, null)
      })
  },

  onChange () {}
}
