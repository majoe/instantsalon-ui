import axios from 'axios';

export const HTTP = axios.create({
  baseURL: process.env.ENDPOINT_API + `reservation/`,
  headers: {
    authorization: 'Token df608d4bab2cf4e48d11162041c8a4cfe081d88a',
    'content-type': 'application/json'
  }
})
