import Vue from 'vue'
import VueRouter from 'vue-router'
import auth from '../auth/auth'
import Login from '../auth/components/Login'
import About from '../common/components/About'
import ServiceSearch from '../reservation/components/ServiceSearch'
import ServiceDetail from '../reservation/components/ServiceDetail'

Vue.use(VueRouter)

function requireAuth (to, from, next) {
  if (!auth.loggedIn()) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
}

export default new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/about', component: About },
    { path: '/dashboard', component: ServiceSearch, beforeEnter: requireAuth },
    { path: '/services/:id', component: ServiceDetail, name: 'ServiceDetail' },
    { path: '/login', component: Login },
    { path: '/logout',
      beforeEnter (to, from, next) {
        auth.logout()
        next('/')
      }
    }
  ]
})
